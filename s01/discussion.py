import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# [SECTION] Create a figure
plt.figure(figsize = (10, 5))
# The '10' is the width of the figure
# The '5', is the height of the figure

# Defining values in x and y axis
x1 = [0, 1, 2, 3, 4]
y1 = [0, 2, 4, 6, 8]

# First Plot
plt.plot(x1, y1, 'g.--', label = 'greenline', markersize = 20, markeredgecolor = 'blue')
plt.legend() # shows the labels of the graph
plt.show() # show the entire graph

# Second plot
x2 = np.arange(0, 4.5, 0.5)

plt.plot(x2, x2 ** 2, 'r', label = 'redline')

font = { 'family': 'serif',
		'color': 'darkred',
		'weight': 'normal',
		'size': 16
		}

plt.title('Marketing sale!', fontdict = font)
plt.xlabel('This is the x-axis')
plt.ylabel('This is y-axis')
plt.xticks([0, 1, 2, 3, 4])
plt.yticks([0, 2, 4, 6, 8, 10, 12, 14, 16])

# saves the graph into our computer
# plt.savefig('linegraph.png', dpi = 100)

plt.legend()
plt.show()


# Third plot
gas = pd.read_csv('./gas_prices.csv') # initialize the csv file of data to be used and put in the variable

plt.figure(figsize = (8,5))

plt.title('Gas Prices', fontdict = { 'family': 'monospace'})
plt.ylabel('US Dollars')
plt.xlabel('Year')

plt.plot(gas.Year, gas.USA, 'b.--', label = 'United States')
plt.plot(gas.Year, gas.Canada, 'rs--', label = 'Canada')
plt.plot(gas.Year, gas['South Korea'], 'm*:', label = 'South Korea')
plt.plot(gas.Year, gas.Australia, 'c+-', label = 'Australia')

search_country = ['USA', 'Canada', 'South Korea', 'Australia']
for country in gas:
	if country in search_country:
		plt.plot(gas.Year, gas[country], marker = '.')

# for country in gas:
# 	if country != 'Year':
# 		plt.plot(gas.Year, gas[country], marker = '*')

plt.legend()
plt.show()

# Fourth Plot - Bar Graph
plt.figure(figsize = (6, 4))

# b. Plot the values needed for the graph as well as the color per bar, make sure it has the same size and shape.
labels = ['Horror', 'Comedy', 'Action', 'Drama', 'Sci_Fi', 'Romance']
values = [18, 30, 38, 24, 12, 59]
bar_color = ['magenta', 'cyan', 'orange', 'pink', 'blue', 'limegreen']

# c. Set the title and labels of x and y axis
plt.title('Top Movie Genre', fontdict = font)

plt.xlabel('movie_genre')
plt.ylabel('people')

# d. bar(x, height) make a bar plot
	# x - for x axis
	# height - for y axis
plt.bar(labels, values, color = bar_color)
plt.show()

# Pie Graph
#plt.pie([left, right]) 
plt.pie([top, bottom])
# It utilizes the pie() function with a '[left, right]' argument which signifies the sides of the pie. The rest of the functionality is same as the line graph and bar graph.