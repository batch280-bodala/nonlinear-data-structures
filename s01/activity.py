import matplotlib.pyplot as plt 
import pandas as pd 
import numpy as np 

x1 = [2008,2009,2010,2011,2012]
y1 = [8,12,10,14,18]
plt.plot(x1, y1, 'g.--', label='movies watched', markersize=20, markeredgecolor='blue')
font = {'family': 'serif', 'color': 'black', 'weight': 'normal', 'size': 16}
plt.title("Movies watched by Kim's family", fontdict=font)
plt.xlabel('Year')
plt.ylabel("Number of Movies")
plt.xticks([2008,2009,2010,2011,2012])
plt.yticks([6,8,10,12,14,16,18,20])
plt.legend()
plt.show()

years = [2008,2009,2010,2011,2012,2013]
island = [700,500,550,645,300,200]
eden = [650,700,480,800,200,300]
plt.plot(years, island, 'g.--', label='Island Middle School',markersize=20,markeredgecolor='blue')
plt.plot(years, eden, 'c+-', label='Eden Middle School',markersize=20,markeredgecolor='red')
plt.title("Island vs Eden collection of Aluminium Beverage Cans", fontdict=font)
plt.xlabel('Year')
plt.ylabel("Weight of cans in pounds")
plt.xticks([2008,2009,2010,2011,2012,2013])
plt.yticks([200,300,400,500,600,700,800,900])
plt.legend()
plt.show()

insects = ['Ant', 'Lady Bug', 'Grasshopper', 'Spider']
count = [3,10,5,8]
bar_color = ['black', 'orange', 'green', 'grey']
plt.barh(insects, count, color = bar_color )
plt.title("Jason's Insects", fontdict=font)
plt.xlabel('Total Count')
plt.ylabel("Insect Type")
plt.xticks([1,2,3,4,5,6,7,8,9,10])
plt.show()

activity = ['Sandcastle building','Volleyball','Collecting seashells','Surfing','Kite flying']
percent = [21,15,17,30,17]
plt.pie(percent, labels = activity)
plt.title("Mrs. Carolyn's Class Favorite Beach Activities")
plt.show()