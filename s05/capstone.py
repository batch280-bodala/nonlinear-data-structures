import matplotlib.pyplot as plt

student_list = ['belle', 'ariel', 'aurora', 'anna', 'tiana', 'merida']
student_grade = [60, 80, 95, 85, 89, 97]

def continue_to_menu():
    print(input('\nPress ENTER to continue'))
    main_menu()

def main_menu():
    menu = """
    [program begins]
    Menu:
    1. Add a new student & grade
    2. Print all students
    3. Print all grades
    4. Lookup a student's grade
    5. Find student with highest grade
    6. Find student with lowest grade
    7. Generate graph
    8. Quit
    """
    print(menu)
    choice = input("Choose an option: ")

    if choice == '1':
        add_grade()
    elif choice == '2':
        print_students()
    elif choice == '3':
        print_grades()
    elif choice == '4':
        lookup_grade()
    elif choice == '5':
        find_highest_grade()
    elif choice == '6':
        find_lowest_grade()
    elif choice == '7':
        generate_graph()
    elif choice == '8':
        exit()
    else:
        print("Invalid choice. Please try again.")
        continue_to_menu()

def add_grade():
    name = input("New student name: ")
    grade = int(input("New student grade: "))
    student_list.append(name)
    student_grade.append(grade)
    print(f"Added {name} with grade {grade} to the gradebook.")
    continue_to_menu()

def print_students():
    print("All students: ", student_list)
    continue_to_menu()

def print_grades():
    print("All grades: ", student_grade)
    continue_to_menu()

def lookup_grade():
    name = input("Look up which student? ")
    if name in student_list:
        index = student_list.index(name)
        print(f"Student {name} has grade {student_grade[index]}")
    else:
        print("Student does not exist.")
    continue_to_menu()

def find_highest_grade():
    max_grade = max(student_grade)
    index = student_grade.index(max_grade)
    print("The student with the highest grade is", student_list[index])
    print("His or her grade is", max_grade)
    continue_to_menu()

def find_lowest_grade():
    min_grade = min(student_grade)
    index = student_grade.index(min_grade)
    print("The student with the highest grade is", student_list[index])
    print("His or her grade is", min_grade)
    continue_to_menu()

def generate_graph():
    categories = ['60-70', '71-80', '81-90', '91-95', '96-100']
    grade_ranges = [(60, 70), (71, 80), (81, 90), (91, 95), (96, 100)]
    category_counts = [0] * len(categories)

    for grade in student_grade:
        for i, (low, high) in enumerate(grade_ranges):
            if low <= grade <= high:
                category_counts[i] += 1
                
    plt.figure(figsize=(8, 8))
    plt.pie(category_counts, labels=categories, autopct='%1.1f%%', startangle=140)
    plt.title("Grades of all students")
    plt.show()
    continue_to_menu()
           
main_menu()