import pandas as pd

artist = dict(
	{
		'artist_name':'Suga',
		'company': 'BigHit Entertainment',
		'age': 30
	}
)

band = dict(
	{
		'band_name':'BTS',
		'years_active': 10,
		'hit_songs': ['Dynamite', 'Mic Drop', 'Fire'],
		'is_active': True
	}
)

merged_dict = {**artist, **band}

print(merged_dict, "\n")

student_info = {
	'Class': {
		'student': {
			'name': 'joon',
			'marks': {
				'physics': 85,
				'history': 90
			}
		}
	}
}

print('History grade:', student_info['Class']['student']['marks']['history'], "\n")

personal_info = {
	'name': 'Jean',
	'age': 31,
	'salary': 45000,
	'city': 'Seoul'
}
# Keys to extract
keys = [ 'name', 'salary']

new_dict = dict()

for key in keys:
	new_dict.update({key : personal_info[key]})

print(new_dict, "\n")

employees = {
	'emp1': {'full_name': 'Any Santiago', 'salary': 45000},
	'emp2': {'full_name': 'Charles Boyle', 'salary': 50000},
	'emp3': {'full_name': 'Rose Diaz', 'salary': 40000},
	'emp4': {'full_name': 'Jake Peralta', 'salary': 45000}
}

employees['emp4']['salary'] = 55000
print(employees, "\n")

exam_data = {
	'name': ['Dwight', 'Michael', 'Jim', 'pam', 'Andy'],
	'score': [12.5,5,10,16.5,9],
	'attempts': [1,3,2,1,3],
	'quality': ['yes','no','yes', 'yes', 'no']
}

data_frame = pd.DataFrame(exam_data, index=['a','b','c','d','e'])
print(data_frame, "\n")
print(data_frame.loc['c',:])