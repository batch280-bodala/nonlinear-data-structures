from heapq import heapify, heappush, heappop, nlargest, nsmallest

min_heap = []

heapify(min_heap)

def print_min_heap():
	print("Min-heap elements: ")

	for i in min_heap:
		print(i, end=' ')
	print('\n')

heappush(min_heap, 10)
heappush(min_heap, 20)
heappush(min_heap, 30)
heappush(min_heap, 40)
heappush(min_heap, 50)

print('min heap: ' , min_heap)
print('largest values: ', nlargest(3, min_heap))
print('deleted item: ', heappop(min_heap))
print('new min_heap: ', min_heap ) 

max_heap = []

heapify(max_heap)

heappush(max_heap, -1 * 10)
heappush(max_heap, -1 * 40)
heappush(max_heap, -1 * 70)
heappush(max_heap, -1 * 80)
heappush(max_heap, -1 * 30)

def print_max_heap():
	print("Max-heap elements: ")
	for i in max_heap:
		print((-1 * i), end=' ')
	print('\n')

print('max heap: ', max_heap)
print('2 largest: ', nlargest(2, max_heap))
print('3 smallest values: ', nsmallest(3, max_heap))

print('sorted heap: ', end='')

def print_max_heap_sorted():
    sorted_max_heap = []
    
    while max_heap:
        element = heappop(max_heap)
        sorted_max_heap.append(element)
    
    for i in sorted_max_heap:
        print(i, end=' ')

print_max_heap_sorted()